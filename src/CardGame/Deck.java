package CardGame;
import java.util.Random;
import java.util.ArrayList;
public class Deck {
	private ArrayList <Card> Passport;
	//making private arraylist named passport 
	//int x;
	public Deck(){
		Passport = new ArrayList<Card>();
		//^setting the array list full of actual card objects
		for(int i = 0; i < 4; i++){
			for(int j = 2; j<15; j++ ){
				Passport.add(new Card(j, i));
			} 
		}
	}

	public Card DealCard(){
		if(!Passport.isEmpty()){
			//exclamation point is a NOT statement, 
			return Passport.remove(0);
			//the zero indicates the top card, lowest position in the array
		}
		return null;
	}
	public void PutBackCard (Card cardins){
		Passport.add(cardins);
		//whoever wins is putting the card back into their deck
	}
	Random rand =new Random();
	int random;
	public void shuffle (){
		for(int q = 0; q < Passport.size(); q++){
			random = rand.nextInt(53);
			//make a random number, sets it equal to random from zero to 52;
			Card a = Passport.get(q);
			Card b = Passport.get(random);
			Passport.set(random,a);
			Passport.set(q, b);
		}
	}

}

