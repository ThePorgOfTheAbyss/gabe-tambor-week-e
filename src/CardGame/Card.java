package CardGame;

public class Card {
	// public makes it Accessible by other classes
	private int Value;
	private int Suit;

	public Card(int Value, int Suit){
		//constructors establish the instances
		this.Value=Value;
		//this. equals instance of the variable
		this.Suit=Suit;
	}
	public int getValue(){
		return Value;
		//this is distributing the value to the other class, then returning it to this class
	}

	public int getSuit(){
		return Suit; 
	}
	public String toString(){
		// toString in this case means nothing-simply the name of the method. 
		//converts an integer to a String
		if(Suit == 0){
			if(Value == 11){
				return "Jack of Clovers";
			}
			if(Value == 12){
				return "Queen of Clovers";
			}
			if(Value == 13){
				return "King of Clovers";
			}
			if(Value == 14){
				return "Ace of Clovers";
			}
			else{
				return Value+" of Clovers";
			}
		}
		if(Suit == 1){
			if(Value == 11){
				return "Jack of Hearts";
			}
			if(Value == 12) {
				return "Kween of Hartz";
			}
			if(Value == 13) {
				return "King of Hearts";
			}
			if(Value == 14) {
				return "Ace of Hearts";
			}
			else {
				return Value + " of Hearts";
			}
		}
		if(Suit == 2){
			if(Value == 11){
				return "Jack of Spades";
			}
			if(Value == 12) {
				return "Queen of Spades";
			}
			if(Value == 13) {
				return "King of Spades";
			}
			if(Value == 14) {
				return "Ace of Spades";
			}
			else{
				return Value + " of Spades";
			}
		}
		return toString();
	}


}
