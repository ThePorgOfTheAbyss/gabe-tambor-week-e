public class Time{
	private int hours;
	private int minutes;
	private int seconds;


	public Time() {
		//default constructor
		this.hours = 0;
		this.minutes = 0;
		this.seconds = 0;
	}

	public Time(int hours, int minutes, int seconds) {
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}

	public int getHours() {
		return this.hours;
	}

	public int getMinutes() {
		return this.minutes;
	}

	public int getSeconds() {
		return this.seconds;
	}
	//setters take info
	public void setHours(int hours) {
		if(hours >24) {
			this.hours = 24;
		}else {
			this.hours = hours;
		}

	}

	public void setMinutes(int minutes) {
		if(minutes>59) {
			this.hours++;
			this.minutes=60;
		}else {
			this.minutes = minutes;
		}
	}

	public void setSeconds(int seconds) {
		if(seconds>60) {
			this.minutes++;
			this.seconds=60;
		}else {
			this.seconds = seconds;
		}

	}

	//	public void setTime(int hours, int minutes, int seconds) {
	//		this.hours = hours;
	//		this.minutes = minutes;
	//		this.seconds = seconds;
	//	}

	public void setTime(String hms) { // hh:mm:ss (00:00:00) {"00","00","00"}
		String[] info = hms.split(":");
		this.hours = Integer.parseInt(info[0]);
		//taking from the array
		this.minutes = Integer.parseInt(info[1]);
		this.seconds = Integer.parseInt(info[2]);
	} 

	public String toString() {
		return this.hours+":"+this.minutes+":"+this.seconds;
	}

	public void tick(){
		if(this.seconds == 0) {
			if(this.minutes == 0) {
				this.hours--;
				this.minutes =59;
			}else {
				this.minutes--;
			}
			this.seconds = 59;
		}else {
			seconds--;
		}
	}
}