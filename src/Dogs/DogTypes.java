package Dogs;

public class DogTypes {
	String name;
	int size;
	int age;
	String color;
	public DogTypes(String name, int size, int age, String color){
		this.name=name;
		this.size=size;
		this.age=age;
		this.color=color;
	}
	void printinfo(){
		System.out.println(name);
		System.out.println(size);
		System.out.println(age);
		System.out.println(color);
	}
	public String getName(){
		return name;
	}
	public int getAge(){
		return age;
	}
	public int getSize(){
		return size;
	}
	public String getColor(){
		return color;
	}
	void setName(String newname){
		name = newname;
	}
	void setSize(int newsize){
		size = newsize;
	}
	void setAge(int newage){
		age = newage;
	}
	void SetColor(String newcolor){
		color = newcolor;
	}
}


