package dowlandzPeasantGame;

import dowlandzPeasantGame.Move;

public class Peasant implements Player{
	int mana;
	int health;
	public Peasant(int mana, int health){
		this.mana = mana;
		this.health = health;
	}
	public Move[] getMoves(){
		Move[] PesMove = new Move[7];
		PesMove[0] = new Move(1, 1,"Submissive Strike");
		PesMove[1] = new Move(2, 3, "French Tactical Baugette");
		PesMove[2] = new Move(3, 5, "The Enlightenment of Jared Diamond And His Observation of The Opressive System Of Social Hieracrhy And Disproporitionate Wealth And Power Of The Elite As A Result Of The Neolithic Revolution And The Development Of Agriculture");
		PesMove[3] = new Move(4, 7, "Entropy");
		PesMove[4] = new Move(5, 8, "Imprisonment in the Pokemon Gulag");
		PesMove[5] = new Move(8, 11, "Lawnmower Influenza and German Gummy Bears");
		PesMove[6] = new Move(12, 17, "Death Balloons Filled With Invading Peasant Infantry and Various Ikea Rugs");
		
		return PesMove;
	}

	public int getMana(){
		return mana;
	}
	public int getHealth(){
		return health;
	}
	public void dealDamage(int damage){
		health-=damage;
	}
	public void refreshMana(){
		mana = 10;
	}
	
}
