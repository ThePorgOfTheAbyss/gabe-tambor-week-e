package dowlandzPeasantGame;
import dowlandzPeasantGame.Move;
import java.lang.Math;
public class Knight {
	int mana;
	int health;
	public Knight(int mana, int health){
		this.mana = mana;
		this.health = health;
	}
	public Move[] getMoves(){
		Move[] PesMove = new Move[7];
		PesMove[0] = new Move(3, 5,"Valiant Charge");
		PesMove[1] = new Move(5, 9, "Honor and Glory");
		PesMove[2] = new Move(6, 12, "Bathing in Potatoes");
		PesMove[3] = new Move(10, 19, "Steven Fink: Queen of England");
		PesMove[4] = new Move(15, 30, "Cucumber Artilery and Metalica");
		PesMove[5] = new Move(22, 56, "Microscopic Bazzoka-Carrying Salmon");
		PesMove[6] = new Move(30, 74, "Propoganda for Mustard Companies");

		return PesMove;
	}

	public int getMana(){
		return mana;
	}
	public int getHealth(){
		return health;
	}
	public void dealDamage(int damage){
		double rand = Math.random();
		int chance = (int)(rand*2%2);
		health-=damage+1+chance;
	}
	public void refreshMana(){
		mana = 100;
	}

}


