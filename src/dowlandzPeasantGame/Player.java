package dowlandzPeasantGame;

public interface Player {
public int getHealth();
public Move[] getMoves();
public int getMana();
public void dealDamage(int damage);
public void refreshMana();
}
