
public class BinarySearchTreeNode {
	int key;
	BinarySearchTreeNode LeftNode;
	BinarySearchTreeNode RightNode;
	
	public BinarySearchTreeNode(int key){
		this.key = key;
	}
public int getKey(){
	return key;
}
public BinarySearchTreeNode getLeftNode(){
	return LeftNode;
}
public BinarySearchTreeNode getRightNode(){
	return RightNode;
}
public void setLeftNode(BinarySearchTreeNode LeftNode){
	this.LeftNode = LeftNode;
}
public void setRightNode(BinarySearchTreeNode RightNode){
	this.RightNode = RightNode;
}
}

