import java.util.HashMap;
	public class DynamicFibonacci {
//Dynamic programming-to solve a large problem, use the solutions of smaller problems to solve the main problems
	//Involves storing the solutions for the subproblems
	public static void main(String[] args) {
		HashMap<Integer, Integer> map = new HashMap<>();
		System.out.println(Fibo(67, map));
		int e;
		int ee;
		int eee;
		int eeee;
		int eeeee;
	}
	public static int Fibo (int an, HashMap <Integer, Integer> map){
		if ( map.get(an) != null){
			return map.get(an);
		}
		if(an ==  0){
			return 1;
		}
		else if(an == 1){
			return 1;
		}
		else{
			int Store =Fibo(an-1, map)+Fibo(an-2, map);
			map.put(an, Store);
			return Store;
		}

	}
}


