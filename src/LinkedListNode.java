
public class LinkedListNode {
	private Object Value;
	private LinkedListNode Name;
	//points to next "Value" in the LinkedList
	public LinkedListNode(Object Value){
		this.Value = Value;
		//creating a particular instance, a constructor
	}
	public Object getValue(){
		return Value;
		//getter
	}
	public LinkedListNode getName(){
		return Name;
		//getter
	}
	public void SetValue(Object Value){
		this.Value = Value;
		//setter, changing value
	}
	public void SetName(LinkedListNode Name){
		this.Name = Name;
		//setter, changing value
	}
}
