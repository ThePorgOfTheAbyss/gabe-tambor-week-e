package FlashCardObjectPractice;

public class FlashCard2 {


	
		// public makes it Accessible by other classes
		private double Number;
		private String Term;
		private String Definition;

		public FlashCard2(double Number, String Term, String Definition){
			//constructors establish the instances
			this.Number=Number;
			//specific
			this.Term=Term;
			this.Definition=Definition;
		}
		public double getNumber(){
			return Number;
			//this is distributing the value to the other class, then returning it to this class
		}

		public String getTerm(){
			return Term;
		}
		public String getDefinition(){
			return Definition;
		}
	}
	






