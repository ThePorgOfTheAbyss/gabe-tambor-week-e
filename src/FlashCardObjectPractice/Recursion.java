
package FlashCardObjectPractice;
import java.util.Scanner;
public class Recursion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(step(1, 3, 1, 0));
	}
	public static int step(int n, int m, int o, int p){
		//n is the base number
		//m is the exponent
		//o is the n times the current exponent, the current output
		//p is the amount of times the method has run
		if(p == m){
			return (o);

		}
		else{
			o = (n*o)+1;
			p = p+1;
			return step(n, m, o, p);
			//
		}

	}



