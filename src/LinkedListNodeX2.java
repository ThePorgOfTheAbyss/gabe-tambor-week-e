
public class LinkedListNodeX2 {
private Object val;
private LinkedListNodeX2 proceding;
private LinkedListNodeX2 following;

public LinkedListNodeX2(Object val, LinkedListNodeX2 proceding, LinkedListNodeX2 following) {
		this.val = val;
		this.proceding = proceding; 
		this.following = following;
}

public Object getVal() {
	return val;
}
public LinkedListNodeX2 getProceding() {
	return proceding;
}
public LinkedListNodeX2 getFollowing() {
	return following;
}

public void setVal (Object b){
	this.val = b;
}
public void setProceding (LinkedListNodeX2 c) {
this.proceding = c;
}
public void setFollowing (LinkedListNodeX2 d) {
	this.following = d;
}
}

