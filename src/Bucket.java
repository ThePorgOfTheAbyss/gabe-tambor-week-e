
public class Bucket {
	public int val;
	public String key;
	public Bucket(int val, String key) {
		this.val = val;
		this.key = key;
	}
	public int getVal() {
		return val;
	}
	public String getKey(){
		return key;
	}
	public  void setVal(int val) {
		this.val = val;
	}
	public void setKey(String key) {
		this.key = key; 
	}
}
