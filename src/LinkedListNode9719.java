
public class LinkedListNode9719 {
	int value;
	LinkedListNode9719 nextNode;
	public LinkedListNode9719(int value, LinkedListNode9719 nextNode){
		this.value = value;
		this.nextNode = nextNode;
	}
	public int getValue(int value){
		return value;
	}
	public LinkedListNode9719 getNextNode(LinkedListNode9719 nextNode){
		return nextNode;
	}
	public void setNextNode(LinkedListNode9719 nextNode){
		this.nextNode = nextNode;
	}
}
