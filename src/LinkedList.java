import java.util.jar.Attributes.Name;
//useful notes/vocab:
//index-what position in the node chain a node is
//head-the beginning node, index zero
//driver-the class in which a main method is used to run the program
public class LinkedList {
	private LinkedListNode Head;
	public LinkedList(){
		//new method, i as its parameter
		
		//constructor, indicating that the first node has a value of i, the node at index 0
	}
	public void AddNode(Object i){
		if(Head == null){ 
			Head = new LinkedListNode(i);
		}
		//adding a new index to the chain
		else if(Head.getName() == null){
			//if the first node doesn't have a next node, empty
			Head.SetName(new LinkedListNode(i));
			//a new node/index is being attached to head
		}
		else{
			RecursiveNode(Head.getName(), i);
			//checking to see if there is a next node recursively until it finds the end
		}
	}
	private void RecursiveNode(LinkedListNode Nodey, Object i){
		//creating a recursion function, with a "counter"/node your checking in the list, i is the value of the new node
		if(Nodey.getName() == null){
			//if the counter is finished no "next node"
			Nodey.SetName(new LinkedListNode(i));
			//a new node is being created with a value of i
		}
		else{
			RecursiveNode(Nodey.getName(), i);
			//otherwise, it looks at the next one, checking its value
		}

	}
	public Object getIndex(int i){
		if(i == 0){
			return Head.getValue();
		}
		else{
			return RecursiveGet(i, Head.getName(), 1);
		}
	}
	public int findIndex(Object i){
		if(i.equals(Head.getValue())){
			return 0; 
		}
		else{
			return RecursiveFind(i, Head.getName(), 1);
		}
	
	}
	private int RecursiveFind(Object i, LinkedListNode Nodelee, int Counter){
		if(Nodelee.getValue().equals(i)){
			return Counter;
		}
		else{
			Counter++;
			Nodelee = Nodelee.getName();
			return RecursiveFind(i, Nodelee, Counter);
		}
	}
	private Object RecursiveGet(int i, LinkedListNode Nodelee, int counter){
		if(counter == i){
			return Nodelee.getValue();
		}
		else{
			counter++;
			return RecursiveGet(i, Nodelee.getName(), counter);
		}
	}
	private void Remove(int i, int counter, LinkedListNode Nodelee){
		if(counter == i){
			System.out.println(Nodelee.getValue());
			Nodelee = Nodelee.getName();
			System.out.println(Nodelee.getValue());
			if(counter == 0){
				Head = Nodelee;
			}
		}
		else{
			counter++;
			Remove(i, counter, Nodelee);
		}
	}
	public void remove(int i){
		Remove(i, 0, Head);
		System.out.println("m");
	}
	public int Size(){
		return lowercasesize(0, Head);
	}
	private int lowercasesize(int counter, LinkedListNode Nodelee ){
		counter++;
		if(Nodelee.getName()== null){
			return counter;
		}
		else{
			return lowercasesize(counter, Nodelee.getName());
		}
			//name is bassiclally nextnode
	}
	//takes in user index, public method, call the remove method, set up remove method for you
}

//CALLING IS TAKING THE NAME OF THE METHOD (OPEN PARENTHASEES-INSET PARAMETERS)